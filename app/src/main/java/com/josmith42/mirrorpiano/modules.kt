package com.josmith42.mirrorpiano

import android.content.Context
import android.media.midi.MidiManager
import com.josmith42.mirrorpiano.core.MpMidiManager
import com.josmith42.mirrorpiano.core.MpMidiManagerImpl
import com.josmith42.mirrorpiano.core.MirrorPiano
import com.josmith42.mirrorpiano.core.MirrorPianoImpl
import com.josmith42.mirrorpiano.core.json.JsonSerializer
import com.josmith42.mirrorpiano.core.json.MpMidiDeviceInfoJson
import com.josmith42.mirrorpiano.viewmodel.MidiDeviceViewModel
import com.josmith42.mirrorpiano.viewmodel.MirrorPianoViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single<MpMidiManager> { MpMidiManagerImpl(get()) }
    single { JsonSerializer() }

    factory {
        val context: Context = get()
        context.getSystemService(Context.MIDI_SERVICE) as MidiManager
    }
    factory { MpMidiDeviceInfoJson(jsonSerializer = get()) }
    factory<MirrorPiano> { MirrorPianoImpl(midiManager = get()) }

    viewModel { MidiDeviceViewModel(mpMidiManager = get()) }
    viewModel { params ->
        MirrorPianoViewModel(
            mirrorPiano = get(),
            device = params.get(),
        )
    }
}
