package com.josmith42.mirrorpiano

import android.app.Application
import com.josmith42.mirrorpiano.debugStubs.debugModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class MirrorPianoApp: Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidContext(this@MirrorPianoApp)
            if (BuildConfig.DEBUG_STUB) {
                modules(appModule, debugModule)
            }
            else {
                modules(appModule)
            }
        }
    }
}