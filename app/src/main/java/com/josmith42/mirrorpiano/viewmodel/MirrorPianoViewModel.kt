package com.josmith42.mirrorpiano.viewmodel

import androidx.lifecycle.ViewModel
import com.josmith42.mirrorpiano.core.MirrorPiano
import com.josmith42.mirrorpiano.core.MpMidiDeviceInfo
import timber.log.Timber

class MirrorPianoViewModel(
    val mirrorPiano: MirrorPiano,
    val device: MpMidiDeviceInfo
) : ViewModel() {
    init {
        Timber.d("Opening device: $device")
        mirrorPiano.openDevice(device)
    }

    fun close() {
        mirrorPiano.close()
    }
}