package com.josmith42.mirrorpiano.viewmodel

import androidx.lifecycle.ViewModel
import com.josmith42.mirrorpiano.core.MpMidiManager

class MidiDeviceViewModel(mpMidiManager: MpMidiManager) : ViewModel() {
    val devices = mpMidiManager.midiDevices
}