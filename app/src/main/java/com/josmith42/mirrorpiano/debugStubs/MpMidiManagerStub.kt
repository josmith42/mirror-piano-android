package com.josmith42.mirrorpiano.debugStubs

import com.josmith42.mirrorpiano.core.MpMidiManager
import com.josmith42.mirrorpiano.core.MpMidiDeviceInfo
import kotlinx.coroutines.flow.MutableStateFlow

private fun makeDeviceInfo(product: String, manufacturer: String) =
    MpMidiDeviceInfo(
        name = "$manufacturer $product",
        product = product,
        manufacturer = manufacturer
    )


val stubDeviceList = listOf(
    makeDeviceInfo("Nord Grand", "Clavia"),
    makeDeviceInfo("MP11SE", "Kawai"),
)

class MpMidiManagerStub: MpMidiManager {
    override val midiDevices = MutableStateFlow(stubDeviceList)
    override fun openDevice(deviceId: String) = stubDeviceList.first { it.product == deviceId }
}