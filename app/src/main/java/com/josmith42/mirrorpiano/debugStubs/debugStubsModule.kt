package com.josmith42.mirrorpiano.debugStubs

import com.josmith42.mirrorpiano.core.MirrorPiano
import com.josmith42.mirrorpiano.core.MpMidiManager
import org.koin.dsl.module

val debugModule = module {
    single<MpMidiManager> { MpMidiManagerStub() }
    factory<MirrorPiano> { MirrorPianoStub() }
}
