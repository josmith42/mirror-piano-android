package com.josmith42.mirrorpiano.debugStubs

import com.josmith42.mirrorpiano.core.MirrorPiano
import com.josmith42.mirrorpiano.core.MpMidiDeviceInfo
import timber.log.Timber

class MirrorPianoStub: MirrorPiano {
    override fun openDevice(device: MpMidiDeviceInfo) {
        Timber.d("[STUB]openDevice($device)")
    }
    override fun close() {
        Timber.d("[STUB]close()")
    }
}