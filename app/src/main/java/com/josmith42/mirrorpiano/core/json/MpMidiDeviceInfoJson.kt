package com.josmith42.mirrorpiano.core.json

import com.josmith42.mirrorpiano.core.MpMidiDeviceInfo

class MpMidiDeviceInfoJson(jsonSerializer: JsonSerializer) {
    private val adapter = jsonSerializer.adapter<MpMidiDeviceInfo>()

    fun toJson(mpMidiDeviceInfo: MpMidiDeviceInfo): String = adapter.toJson(mpMidiDeviceInfo)
    fun fromJson(json: String) = adapter.fromJson(json)
}