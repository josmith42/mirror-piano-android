package com.josmith42.mirrorpiano.core

import android.media.midi.MidiDeviceInfo
import android.media.midi.MidiManager
import android.os.Handler
import android.os.Looper
import timber.log.Timber

interface MirrorPiano {
    fun openDevice(device: MpMidiDeviceInfo)
    fun close()
}

class MirrorPianoImpl(private val midiManager: MidiManager) : MirrorPiano {

    private var nativeMidiDevicePtr = 0L

    override fun openDevice(device: MpMidiDeviceInfo) {
        val midiDeviceInfo = midiManager.devices.firstOrNull {
            device.manufacturer == it.manufacturer &&
                    device.product == it.product
        }

        if (midiDeviceInfo == null) {
            Timber.e("Couldn't find device $device")
            return
        }
        val input =
            midiDeviceInfo.ports.firstOrNull { it.type == MidiDeviceInfo.PortInfo.TYPE_INPUT }
        val output =
            midiDeviceInfo.ports.firstOrNull { it.type == MidiDeviceInfo.PortInfo.TYPE_OUTPUT }

        if (input == null || output == null) {
            Timber.e("Cannot find I/O ports for device: $device")
            return
        }

        midiManager.openDevice(midiDeviceInfo, { midiDevice ->
            Timber.d("Opening device ${midiDevice.info.name} with input: ${input.portNumber} and output ${output.portNumber}")
            nativeMidiDevicePtr = NativeMidi.openMidi(midiDevice, input.portNumber, output.portNumber)
            Timber.d("NativePtr: ${nativeMidiDevicePtr.toString(16)}")
        }, Handler(Looper.getMainLooper()))

    }

    override fun close() {
        NativeMidi.closeMidi(nativeMidiDevicePtr)
    }
}

val MidiDeviceInfo.name
    get() = properties[MidiDeviceInfo.PROPERTY_NAME] as String

val MidiDeviceInfo.product
    get() = properties[MidiDeviceInfo.PROPERTY_PRODUCT] as String

val MidiDeviceInfo.manufacturer
    get() = properties[MidiDeviceInfo.PROPERTY_MANUFACTURER] as String
