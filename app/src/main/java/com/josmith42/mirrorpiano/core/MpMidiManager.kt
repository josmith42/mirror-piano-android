package com.josmith42.mirrorpiano.core

import android.media.midi.MidiDeviceInfo
import android.media.midi.MidiManager
import android.media.midi.MidiManager.DeviceCallback
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

interface MpMidiManager {
    val midiDevices: StateFlow<List<MpMidiDeviceInfo>>

    fun openDevice(deviceId: String): MpMidiDeviceInfo
}

class MpMidiManagerImpl(private val midiManager: MidiManager) : MpMidiManager, DeviceCallback() {

    override val midiDevices = MutableStateFlow(mapDevices())
    override fun openDevice(deviceId: String): MpMidiDeviceInfo {
        val device = midiManager.devices.firstOrNull {
            it.name == deviceId
        } ?: throw IllegalStateException("device not found with name: $deviceId")
        return mapDevice(device)
    }

    init {
        midiManager.registerDeviceCallback(this, null)
    }

    override fun onDeviceAdded(device: MidiDeviceInfo?) {
        updateDevices()
    }

    override fun onDeviceRemoved(device: MidiDeviceInfo?) {
        updateDevices()
    }

    private fun updateDevices() {
        midiDevices.value = mapDevices()
    }

    private fun mapDevices() = midiManager.devices.map(this::mapDevice)

    private fun mapDevice(midiDeviceInfo: MidiDeviceInfo) = midiDeviceInfo.run {
        MpMidiDeviceInfo(
            name = name,
            product = product,
            manufacturer = manufacturer
        )
    }
}