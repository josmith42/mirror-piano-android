package com.josmith42.mirrorpiano.core

data class MpMidiDeviceInfo(
    val name: String,
    val product: String,
    val manufacturer: String,
)
