package com.josmith42.mirrorpiano.core

import android.media.midi.MidiDevice

class NativeMidi {
    companion object {
        init {
            System.loadLibrary("amidi")
            System.loadLibrary("mirror_piano_rust")
            initLogging()
        }
        @JvmStatic
        external fun initLogging()

        @JvmStatic
        external fun openMidi(device: MidiDevice, inputPort: Int, outputPort: Int): Long

        @JvmStatic
        external fun closeMidi(devicePtr: Long)
    }
}