package com.josmith42.mirrorpiano.ui

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.josmith42.mirrorpiano.R
import com.josmith42.mirrorpiano.core.json.MpMidiDeviceInfoJson
import com.josmith42.mirrorpiano.viewmodel.MirrorPianoViewModel
import org.koin.androidx.compose.inject
import org.koin.androidx.compose.viewModel
import org.koin.core.parameter.parametersOf

@Composable
fun MirrorPianoScreen(
    deviceJson: String?,
    backPressed: () -> Unit
) {
    if (deviceJson != null) {
        val json: MpMidiDeviceInfoJson by inject()
        val device = json.fromJson(deviceJson)
        val viewModel: MirrorPianoViewModel by viewModel { parametersOf(device) }
        val navigateUp = {
            viewModel.close()
            backPressed()
        }
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text(text = "${stringResource(id = R.string.app_name)} - ${viewModel.device.product}") },
                    navigationIcon = {
                        IconButton(onClick = navigateUp) {
                            Icon(
                                imageVector = Icons.Filled.Close,
                                contentDescription = "Close"
                            )
                        }
                    }
                )
            }
        ) {
            Column(modifier = Modifier.fillMaxSize()) {
                CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                    Text(
                        modifier = Modifier
                            .padding(16.dp)
                            .fillMaxWidth(),
                        textAlign = TextAlign.Center,
                        text = "Mirroring"
                    )
                }
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center,
                    fontSize = 30.sp,
                    text = viewModel.device.name
                )
                CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                    Text(
                        modifier = Modifier.padding(16.dp),
                        textAlign = TextAlign.Center,
                        text = stringResource(id = R.string.local_off_description)
                    )
                }
            }
            BackHandler {
                navigateUp()
            }
        }
    }
    else {
        Text(text = "Error: deviceJson is null")
    }
}