package com.josmith42.mirrorpiano.ui

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.josmith42.mirrorpiano.ui.theme.MirrorPianoTheme

enum class MirrorPianoAppScreen {
    SelectDevice,
    MirrorPiano
}

@Composable
fun MirrorPianoUi() {
    val navController = rememberNavController()

    MirrorPianoTheme {
        // A surface container using the 'background' color from the theme
        Surface(color = MaterialTheme.colors.background) {
            NavHost(
                navController = navController,
                startDestination = MirrorPianoAppScreen.SelectDevice.name
            ) {
                composable(MirrorPianoAppScreen.SelectDevice.name) {
                    SelectDeviceScreen { deviceId ->
                        navController.navigate("${MirrorPianoAppScreen.MirrorPiano.name}/$deviceId")
                    }
                }
                composable(
                    route = "${MirrorPianoAppScreen.MirrorPiano.name}/{deviceId}",
                    arguments = listOf(
                        navArgument("deviceId") { type = NavType.StringType }
                    )
                ) { entry ->
                    val deviceId = entry.arguments?.getString("deviceId")
                    MirrorPianoScreen(deviceId) { navController.navigateUp() }
                }
            }
        }
    }
}