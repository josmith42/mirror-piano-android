package com.josmith42.mirrorpiano.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.josmith42.mirrorpiano.viewmodel.MidiDeviceViewModel
import com.josmith42.mirrorpiano.ui.theme.MirrorPianoTheme
import com.josmith42.mirrorpiano.R
import com.josmith42.mirrorpiano.core.MpMidiDeviceInfo
import com.josmith42.mirrorpiano.core.json.MpMidiDeviceInfoJson
import com.josmith42.mirrorpiano.debugStubs.stubDeviceList
import org.koin.androidx.compose.inject
import org.koin.androidx.compose.viewModel

@Composable
fun SelectDeviceScreen(onDeviceSelected: (String) -> Unit) {
    val midiDeviceViewModel: MidiDeviceViewModel by viewModel()
    SelectDeviceScreen(
        list = midiDeviceViewModel.devices.collectAsState(emptyList()).value,
        onDeviceSelected = onDeviceSelected
    )
}

@Composable
fun SelectDeviceScreen(list: List<MpMidiDeviceInfo>, onDeviceSelected: (String) -> Unit) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = stringResource(id = R.string.select_midi_device)) }
            )
        }
    ) {
        Column {
            DeviceList(list, onDeviceSelected)
        }
    }
}

@Composable
fun DeviceList(list: List<MpMidiDeviceInfo>, onDeviceSelected: (String) -> Unit) {
    if (list.isEmpty()) {
        Column {
            Spacer(modifier = Modifier.weight(1f))
            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                Text(
                    textAlign = TextAlign.Center,
                    text = stringResource(id = R.string.no_midi_devices),
                    modifier = Modifier
                        .weight(3f)
                        .fillMaxSize()
                )
            }
        }
    } else {
        LazyColumn {
            items(list) {
                DeviceItem(it, onDeviceSelected)
            }
        }
    }
}

@Composable
fun DeviceItem(device: MpMidiDeviceInfo, onDeviceSelected: (String) -> Unit) {
    val typography = MaterialTheme.typography
    val json: MpMidiDeviceInfoJson by inject()
    Column(
        modifier = Modifier.clickable {
            onDeviceSelected(json.toJson(device))
        }
    ) {
        Column(Modifier.padding(12.dp)) {
            Text(text = device.product, style = typography.body1)
            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                Text(
                    text = device.manufacturer,
                    style = typography.subtitle1
                )
            }
        }
        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
            Divider(thickness = 1.dp)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LightPreview() {
    Preview(darkTheme = false)
}

@Preview(showBackground = true)
@Composable
fun DarkPreview() {
    Preview(darkTheme = true)
}

@Composable
fun Preview(darkTheme: Boolean) {
    MirrorPianoTheme(darkTheme = darkTheme) {
        SelectDeviceScreen(stubDeviceList) {}
    }
}
