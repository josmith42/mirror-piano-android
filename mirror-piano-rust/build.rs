use std::env;
use std::path::{Path, PathBuf};

extern crate bindgen;

fn main() {
    let android_ndk_home = env::var("ANDROID_NDK_HOME").expect("ANDROID_NDK_HOME environment variable must be defined");

    let build_platform = if cfg!(target_os = "macos") {
        "darwin"
    } else if cfg!(target_os = "linux") {
        "linux"
    } else {
        panic!("Unknown build platform")
    };

    let base_path = format!("{android_ndk_home}/22.1.7171670/toolchains/llvm/prebuilt/{build_platform}-x86_64/sysroot/usr");
    assert!(Path::new(&base_path).is_dir(), "Path `{base_path}` does not exist. Maybe check the value of ANDROID_NDK_HOME (`{android_ndk_home}`)");

    println!("cargo:rustc-link-lib=dylib=amidi");

    println!("cargo:rerun-if-changed=wrapper.h");

    let bindings = bindgen::Builder::default()
        .header("wrapper.h")

        .clang_arg(format!("-I{}/include/amidi", base_path))
        .clang_arg(format!("-I{}/include", base_path))

        .allowlist_function("AMidiDevice_fromJava")
        .allowlist_function("AMidiOutputPort_open")
        .allowlist_function("AMidiDevice_release")
        .allowlist_function("AMidiOutputPort_receive")
        .allowlist_function("AMidiOutputPort_close")
        .allowlist_function("AMidiInputPort_open")
        .allowlist_function("AMidiInputPort_send")
        .allowlist_function("AMidiInputPort_close")

        .allowlist_var("AMIDI_OPCODE_DATA")

        .blocklist_type("jobject")
        .blocklist_type("JNINativeInterface")
        .blocklist_type("JNIEnv")
        .blocklist_type("jstring")
        .blocklist_type("JNIInvokeInterface")
        .blocklist_type("JavaVM")

        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");
    
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Coundn't write bindings");
}