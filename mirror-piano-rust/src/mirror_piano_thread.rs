use crate::midi::{MidiDevice, MidiError, MidiInputPort, MidiOutputPort};
use crate::mirror_piano::MirrorPiano;
use jni::sys::jobject;
use jni::sys::JNIEnv;
use log::debug;
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;

pub struct MirrorPianoThread {
    tx: Sender<Message>,
}

enum Message {
    Quit,
}

impl MirrorPianoThread {
    pub fn start(
        env: *mut JNIEnv,
        midiDeviceObj: jobject,
        input_port: i32,
        output_port: i32,
    ) -> Result<Self, MidiError> {
        let midi_device = MidiDevice::from_java(env, midiDeviceObj)?;
        let (tx, rx): (Sender<Message>, Receiver<Message>) = mpsc::channel();
        midi_thread(midi_device, rx, input_port, output_port);
        Ok(Self { tx: tx.clone() })
    }
}

impl Drop for MirrorPianoThread {
    fn drop(&mut self) {
        self.tx
            .send(Message::Quit)
            .unwrap_or_else(|e| debug!("Error stopping device: {}", e))
    }
}

fn midi_thread(midi_device: MidiDevice, rx: Receiver<Message>, input_port: i32, output_port: i32) {
    thread::spawn(move || {
        debug!("Midi thread started");
        let mut mirror_piano = MirrorPiano::new();
        match open_io(&midi_device, input_port, output_port) {
            Ok((input, output)) => loop {
                match rx.try_recv() {
                    Ok(Message::Quit) => break,
                    Err(_) => process_midi(&mut mirror_piano, &input, &output),
                }
            },
            Err(error) => debug!("Error opening output device: {}", error),
        }
        debug!("Midi thread ended");
    });
}

fn open_io(
    midi_device: &MidiDevice,
    input_port: i32,
    output_port: i32,
) -> Result<(MidiInputPort, MidiOutputPort), MidiError> {
    let input = MidiInputPort::open(midi_device, input_port)?;
    let output = MidiOutputPort::open(midi_device, output_port)?;
    Ok((input, output))
}

fn process_midi(mirror_piano: &mut MirrorPiano, input: &MidiInputPort, output: &MidiOutputPort) {
    match output.receive() {
        Ok(midi_data) => {
            if !midi_data.is_empty() {
                match mirror_piano.feed(&midi_data) {
                    Some(new_message) => {
                        input.send(&new_message)
                            .unwrap_or_else(|e| debug!("Error while sending {}", e))
                    },
                    None => {}
                }
            }
        }
        Err(error_code) => debug!("Error while receiving: {}", error_code),
    }
}