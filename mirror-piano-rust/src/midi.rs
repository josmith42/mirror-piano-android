use crate::midi_bindings::{
    media_status_t, media_status_t_AMEDIA_OK, size_t, ssize_t, AMidiDevice, AMidiDevice_fromJava,
    AMidiDevice_release, AMidiInputPort, AMidiInputPort_close, AMidiInputPort_open,
    AMidiInputPort_send, AMidiOutputPort, AMidiOutputPort_close, AMidiOutputPort_open,
    AMidiOutputPort_receive, AMIDI_OPCODE_DATA,
};
use jni::sys::jobject;
use jni::sys::JNIEnv;
use log::debug;
use std::error::Error;
use std::fmt;
use std::marker::PhantomData;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ErrorType {
    MediaError(media_status_t),
    MessageError(ssize_t),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct MidiError {
    pub status: ErrorType,
}

impl MidiError {
    fn media_error(status: media_status_t) -> Self {
        Self {
            status: ErrorType::MediaError(status),
        }
    }
    fn message_error(error_code: ssize_t) -> Self {
        Self {
            status: ErrorType::MessageError(error_code),
        }
    }
}

impl Error for MidiError {}
impl fmt::Display for MidiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Midi error status: {:?}", self.status)
    }
}

#[derive(Debug)]
pub struct MidiDevice {
    aMidiDevicePtr: *mut AMidiDevice,
}

impl MidiDevice {
    pub fn from_java(env: *mut JNIEnv, midiDeviceObj: jobject) -> Result<Self, MidiError> {
        let mut aMidiDevicePtr: *mut AMidiDevice = std::ptr::null_mut();

        let status = unsafe { AMidiDevice_fromJava(env, midiDeviceObj, &mut aMidiDevicePtr) };
        debug!("after AMidiDevice_fromJava");
        match status {
            #![allow(non_upper_case_globals)]
            media_status_t_AMEDIA_OK => Ok(MidiDevice { aMidiDevicePtr }),
            _ => Err(MidiError::media_error(status)),
        }
    }
}
impl Drop for MidiDevice {
    fn drop(&mut self) {
        // TODO: need to hold onto JNIEnv object somehow
        // unsafe { AMidiDevice_release(self.aMidiDevicePtr) };
    }
}

unsafe impl Send for MidiDevice {}

#[derive(Debug)]
pub struct MidiOutputPort<'a> {
    aMidiOutputPortPtr: *mut AMidiOutputPort,
    phantom: PhantomData<&'a AMidiOutputPort>,
}

impl<'a> MidiOutputPort<'a> {
    pub fn open(midiDevice: &MidiDevice, portNumber: i32) -> Result<Self, MidiError> {
        let mut aMidiOutputPortPtr: *mut AMidiOutputPort = std::ptr::null_mut();
        let result = unsafe {
            AMidiOutputPort_open(
                midiDevice.aMidiDevicePtr,
                portNumber,
                &mut aMidiOutputPortPtr,
            )
        };
        match result {
            #![allow(non_upper_case_globals)]
            media_status_t_AMEDIA_OK => Ok(Self {
                aMidiOutputPortPtr,
                phantom: PhantomData {},
            }),
            _ => {
                debug!("Cannot open output port - {:?}", result);
                Err(MidiError::media_error(result))
            }
        }
    }

    pub fn receive(&self) -> Result<Vec<u8>, MidiError> {
        let mut dataBuffer = vec![0u8; 12];
        let mut opCode: i32 = 0;
        let mut numBytes: size_t = 0;
        let mut timestamp: i64 = 0;
        let num_messages = unsafe {
            AMidiOutputPort_receive(
                self.aMidiOutputPortPtr,
                &mut opCode,
                dataBuffer.as_mut_ptr(),
                dataBuffer.len() as size_t,
                &mut numBytes,
                &mut timestamp,
            )
        };
        if num_messages >= 0 {
            if opCode == AMIDI_OPCODE_DATA as i32 {
                let message = &dataBuffer[0..numBytes as usize];
                Ok(message.to_vec())
            } else {
                Ok(Vec::new())
            }
        } else {
            Err(MidiError::message_error(num_messages))
        }
    }
}

impl<'a> Drop for MidiOutputPort<'a> {
    fn drop(&mut self) {
        unsafe { AMidiOutputPort_close(self.aMidiOutputPortPtr) };
    }
}

#[derive(Debug)]
pub struct MidiInputPort<'a> {
    aMidiInputPortPtr: *mut AMidiInputPort,
    phantom: PhantomData<&'a AMidiInputPort>,
}

impl<'a> MidiInputPort<'a> {
    pub fn open(midiDevice: &MidiDevice, portNumber: i32) -> Result<Self, MidiError> {
        let mut aMidiInputPortPtr: *mut AMidiInputPort = std::ptr::null_mut();
        let result = unsafe {
            AMidiInputPort_open(
                midiDevice.aMidiDevicePtr,
                portNumber,
                &mut aMidiInputPortPtr,
            )
        };
        match result {
            #![allow(non_upper_case_globals)]
            media_status_t_AMEDIA_OK => Ok(Self {
                aMidiInputPortPtr,
                phantom: PhantomData {},
            }),
            _ => {
                debug!("Cannot open input port - {:?}", result);
                Err(MidiError::media_error(result))
            }
        }
    }

    pub fn send(&self, midiMessage: &[u8]) -> Result<(), MidiError> {
        let num_bytes = unsafe {
            AMidiInputPort_send(
                self.aMidiInputPortPtr,
                midiMessage.as_ptr(),
                midiMessage.len().try_into().unwrap(),
            )
        };
        if num_bytes < midiMessage.len().try_into().unwrap() {
            Err(MidiError::message_error(num_bytes))
        } else {
            Ok(())
        }
    }
}

impl<'a> Drop for MidiInputPort<'a> {
    fn drop(&mut self) {
        unsafe { AMidiInputPort_close(self.aMidiInputPortPtr) };
    }
}

#[derive(Debug)]
pub struct MidiMessage {
    pub data: Vec<u8>,
}