use midly::{live::LiveEvent, stream::MidiStream, num::u7, MidiMessage};

pub struct MirrorPiano {
    midi_stream: MidiStream
}

impl MirrorPiano {
    pub fn new() -> Self {
        Self { midi_stream: MidiStream::new() }
    }

    pub fn feed(&mut self, bytes: &[u8]) -> Option<Vec<u8>> {
        let mut out = Vec::new();
        self.midi_stream.feed(bytes, |event| {
            match mirror_event(event) {
                Some(event) => event.write(&mut out).unwrap_or(()),
                None => {}
            }
        });
        if !out.is_empty() {
            Some(out)
        }
        else {
            None
        }
    }

}

fn mirror_event(event: LiveEvent) -> Option<LiveEvent> {
    match event {
        LiveEvent::Midi{ channel, message } => {
            let message = match message {
                MidiMessage::NoteOn{key, vel} => MidiMessage::NoteOn{key:mirror_pitch(key)?, vel},
                MidiMessage::NoteOff{key, vel} => MidiMessage::NoteOff{key:mirror_pitch(key)?, vel},
                MidiMessage::Aftertouch{key, vel} => MidiMessage::Aftertouch{key:mirror_pitch(key)?, vel},
                non_note => non_note
            };
            Some(LiveEvent::Midi{channel, message})
        },
        non_midi => Some(non_midi)
    }
}

fn mirror_pitch(pitch: u7) -> Option<u7> {
    const REFLECTION_POINT: i32 = 62; // D4

    let pitch: i32 = u8::from(pitch) as i32;

    let new_pitch = REFLECTION_POINT * 2 - pitch;
    match new_pitch {
        0..=127 => Option::Some(u7::from(new_pitch as u8)),
        _ => Option::None
    }
}