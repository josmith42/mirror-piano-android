#![cfg(target_os = "android")]
#![allow(non_snake_case)]

mod midi;
mod mirror_piano;
mod mirror_piano_thread;
mod midi_bindings;

extern crate android_logger;
extern crate log;

use crate::mirror_piano_thread::MirrorPianoThread;

use android_logger::Config;
use jni::objects::JClass;
use jni::sys::{JNIEnv, jint, jlong, jobject};
use log::{debug, Level};

#[no_mangle]
pub unsafe extern "C" fn Java_com_josmith42_mirrorpiano_core_NativeMidi_initLogging(
    _: *mut JNIEnv,
    _: JClass,
) {
    init_logging();
}

#[no_mangle]
pub unsafe extern "C" fn Java_com_josmith42_mirrorpiano_core_NativeMidi_openMidi(
    env: *mut JNIEnv,
    _: JClass,
    device: jobject,
    inputPort: jint,
    outputPort: jint
) -> jlong {
    open_midi(env, device, inputPort, outputPort)
}

#[no_mangle]
pub unsafe extern "C" fn Java_com_josmith42_mirrorpiano_core_NativeMidi_closeMidi(
    _: *mut JNIEnv,
    _: JClass,
    devicePtr: jlong
) {
    close_midi(devicePtr)
}

fn init_logging() {
    android_logger::init_once(
        Config::default()
            .with_min_level(Level::Trace)
            .with_tag("mirror_piano_rust"),
    );
}

fn open_midi(
    env: *mut JNIEnv,
    device: jobject,
    inputPort: jint,
    outputPort: jint
) -> jlong {
    match mirror_piano_thread::MirrorPianoThread::start(env, device, inputPort, outputPort) {
        Ok(mirror_piano_thread) => {
            Box::into_raw(Box::new(mirror_piano_thread)) as jlong
        },
        Err(err) => {
            debug!("Error creating mirror piano: {}", err);
            0
        }
    }
}

fn close_midi(
    devicePtr: jlong
) {
    if devicePtr == 0 {
        return
    }
    let devicePtr = devicePtr as *mut MirrorPianoThread;
    let devicePtr = unsafe { Box::from_raw(devicePtr) };
    drop(devicePtr);
}